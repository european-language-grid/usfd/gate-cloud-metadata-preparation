@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7.1')
import groovyx.net.http.*
import static groovyx.net.http.ContentType.JSON

@Grab(group='org.yaml', module='snakeyaml', version='1.25')
import org.yaml.snakeyaml.Yaml

@Grab(group='org.apache.commons', module='commons-text', version='1.8')
import org.apache.commons.text.StringEscapeUtils

def yml = new Yaml()
def conf = new File(args[0]).withInputStream(yml.&load)

def templ = new File(args[1]).getText('UTF-8')

def outFolder = new File(args[2])

def http = new RESTClient("https://cloud.gate.ac.uk")
http.auth.basic conf.gate.elg.cloud.apiKeyId, conf.gate.elg.cloud.apiKeyPassword

def services = http.get(path:"/api/shop/services", contentType:JSON)

new File(args[3]).eachLine("UTF-8") { slug ->
  def svcData = services.data.find { it.endpointUrl.endsWith("/$slug") }
  if(svcData) {
    println "Found service ${svcData}"

    def replacements = [
      today: '2020-02-25',
      slug: slug,
      'human-readable-name': svcData.name,
      'short-desc': svcData.shortDescription,
      selectors: (svcData.defaultAnnotations + svcData.additionalAnnotations).join(', '),
    ]
    new File(outFolder, "${slug}.xml").setText(templ.replaceAll(~('@(' + replacements.keySet().join('|') + ')@')) { StringEscapeUtils.escapeXml10(replacements[it[1]]) }, "UTF-8")
  } else {
    println "Couldn't find a service with slug $slug"
  }
}
