# Deliver metadata for USFD services

USFD = The University of Sheffield

D4.1 document = "European Language Grid: D4.1 Grid Content:
Services, Tools & Components; edition 2020-02-29

# Outline

As per the D4.1 document, deliver metadata for the Gate Cloud services that
USFD are integrating.

Metadata to be delivered according to ELG-SHARE-schema templates
(see References).

## References

Task 4.3 templates?

https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20templates

Schemas:

https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/Schema

## Service Schedule

The list of services is described in D4.1 section 5.3

To generate the list below I referenced those against
the list of GATE Cloud endpoints
(see Appendix).

The list consists of the trailing part of the Gate Cloud
endpoint URL, and the "name" field from the JSON metadata.
The prefix for the Gate Cloud endpoint URL is
`https://cloud-api.gate.ac.uk/process-document`.

    /german-named-entity-recognizer
      German Named Entity Recognizer
    /french-named-entity-recognizer
      French Named Entity Recognizer
    /twitie-langID-pipeline
      Language Identification for Tweets
    /twitie-tokenization-pipeline
      English Tweet Tokenizer
    /twitie-posTagger-pipeline
      Part-of-Speech Tagger for Tweets
    /french-twitter-named-entity-recognizer
      French Named Entity Recognizer for Tweets
    /german-twitter-named-entity-recognizer
      German Named Entity Recognizer for Tweets
    /pos-tagging-and-morphological-analysis
      English Part-of-Speech and Morphology Analyzer
    /measurement-expression-annotator
      Measurement Expression Annotator
    /opennlp-english-pipeline
      OpenNLP English Pipeline
    /opennlp-german-pipeline
      OpenNLP German Pipeline
    /tagger-upos-en-maxent1
      Universal Dependencies POS Tagger for en / English 
    /tagger-pos-cs-maxent1
      Universal Dependencies POS Tagger for cs / Czech
    /tagger-pos-el-maxent1
      Universal Dependencies POS Tagger for el / Greek
    /tagger-pos-es-maxent1
      Universal Dependencies POS Tagger for es / Spanish
    /tagger-pos-lv-maxent1
      Universal Dependencies POS Tagger for lv / Latvian
    /tagger-pos-fr-maxent1
      Universal Dependencies POS Tagger for fr / French
    /yodie-en
      YODIE Named Entity Disambiguation (English)
    /yodie-de
      YODIE Named Entity Disambiguation (German)
    /yodie-fr
      YODIE Named Entity Disambiguation (French)
    /yodie-es
      YODIE Named Entity Disambiguation (Spanish)
    /bio-yodie-mesh
      BioYODIE Named Entity Disambiguation (MeSH only)
    /bio-yodie-snomed
      BioYODIE Named Entity Disambiguation (Snomed only)
    /bio-yodie
      BioYODIE Named Entity Disambiguation
    /sobigdata-brexit
       The \"Brexit Analyzer\" Pipeline
    /environmental-annotator
      DecarboNet Environmental Annotator
    /german-environmental-annotator
      DecarboNet German Environmental Annotator
    /sobigdata-politics
      The Political Futures Pipeline
    /rumour-veracity
      Rumour veracity classifier
    /summa-EN-summarization-pipeline
      SUMMA Text Summarization (EN)
    /summa-ES-summarization-pipeline
      SUMMA Text Summarization (ES)
    /generic-opinion-mining-english
      Generic Opinion Mining (English)
    /twitter-opinion-mining-english
      Twitter Opinion Mining (English)
    /sobigdata-user-classification
      Twitter user classification

## Appendix


### Example Gate Cloud endpoint

If you have `login` and `password` for `cloud.gate.ac.uk` set in
your `~/.netrc` file:

    wget -O - https://cloud.gate.ac.uk/api/shop/services |
    python -m json.tool

otherwise the same but use the `--user` and `--password` options
to `wget`.

### All Endpoints

The shell script in `bin/all-endpoint` produces a list similar
to the one used in this document and was used to prepare it.
